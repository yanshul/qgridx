#ifndef DRAGLABEL_H
#define DRAGLABEL_H
#include <QLabel>

class DraggabelLables : public QLabel
{
    Q_OBJECT

public:
    explicit DraggabelLables (QWidget *parent=0);
    explicit DraggabelLables (const QString &text, QWidget *parent=0);

protected:
    void paintEvent(QPaintEvent*);
    QSize sizeHint() const ;
    QSize minimumSizeHint() const;

};

#endif // DRAGLABEL_H
