#include "sysinterface.h"
#include <QStringList>

#include <QFile>
#include<QStringList>


QString program = "/usr/bin/xrandr";


SysInterface::SysInterface()
{

    if(NULL==m_pSystemRW)
        m_pSystemRW = new SystemReadWrite;


    QFile file("/home/cae/Documents/work/git_repos/draggableicons/xrandr_v.trace");

    if(!file.open(QIODevice::ReadOnly)) {
        //QMessageBox::information(0,"error",file.errorString());
        int x= 0;
    }

    QString filedata= file.readAll();

    m_parseOutputData = construct_meaningful_strings(filedata );

    //int iNumMonitors = getConnected(xData);
    //int iTotalConnectors = xData.count();

    //m_pXMonitorInfo = new XMonitorInfo( m_parseOutputData);
    //m_parseOutputData=read_display_configuration().split(NEWLINE_STRING);

}

QString SysInterface::getXMonitorString(int index){
    return m_parseOutputData[index];
}

QStringList SysInterface::construct_meaningful_strings( QString temp )
{

    //QString temp = content;
    QStringList qstlst = temp.split(SEARCH_STRING);
    QStringList qsMeaningfulList;

    //Merge 0,1
    QString Screen_Description = qstlst[0]+SPACER+SEARCH_STRING+SPACER+qstlst[1];
    int len = Screen_Description .length();
    int index= Screen_Description.lastIndexOf(NEWLINE_STRING);

    QString screen = Screen_Description.left(index);
    int x=screen.length();
    QString leftover = Screen_Description.right(len-index);
    leftover.remove(NEWLINE_STRING);
    qsMeaningfulList << screen;

    QString tmp="";
    for(int x=2; x<qstlst.count(); ++x ) {
        int len = qstlst.at(x).length();
        tmp = qstlst[x];
        if(leftover != "" )
            tmp = leftover+SEARCH_STRING+tmp;

        int newlen = tmp.length();
        int index= tmp.lastIndexOf(NEWLINE_STRING);

        QString screen = tmp.left(index);
        int len0 = screen.length();

        leftover = tmp.right(newlen-index);
        leftover.remove(NEWLINE_STRING);
        qsMeaningfulList << screen;

    }

    return qsMeaningfulList;

}


int SysInterface::getConnected(  ){

    QString tmp="";
    int connected = 0;
    for(int x=0; x<m_parseOutputData.count(); ++x ) {
        tmp = m_parseOutputData[x];
        if(tmp.contains(" connected", Qt::CaseSensitive ))
            connected ++;

    }
    return connected;
}

int SysInterface::getTotalConnectors( ){
    return m_parseOutputData.count();
}


QString SysInterface:: read_display_configuration(){

    QString command = XCOMMAND;
    QStringList flags;
    flags << "-q";


    QString data="";

    m_pSystemRW->ExecuteWaitCommand( program, flags, data );

    return data;
}

int SysInterface::getScreenCount()
{
    QStringList screens = read_display_configuration().split("Screen ");
    return screens.count();

}

QStringList SysInterface:: getConnectedMonitors(){

    QStringList qstlst;

    for( int i=0; i < m_parseOutputData.count(); ++i ){
        QString tmp = m_parseOutputData[i];
        int ipos = tmp.indexOf(SEARCH_STRING);
         if( tmp.at(ipos-1) == SPACER && ipos > -1 ) //assuming connected is preceded by white space
            qstlst << m_parseOutputData[i];
        }

        return qstlst;

}

QStringList SysInterface:: getConnectedMonitorsFirst(){


    int connected_index = 1;

    QStringList tmplst, qstlst;

    for( int j= 0; j < m_parseOutputData.count() ; ++j ) {

        QString tmp = m_parseOutputData[j];

        int ipos = tmp.indexOf(SEARCH_STRING);


         if( tmp.contains("primary") )
         {
             qstlst.insert(0, m_parseOutputData[j] );
         }
         else if( tmp.at(ipos-1) == SPACER && ipos > -1 ){ //assuming connected is preceded by white space

             qstlst.insert( connected_index++, m_parseOutputData[j] );
         }
         else
             tmplst << m_parseOutputData[j];

    }

    qstlst << tmplst;

    return qstlst;
}

