/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtWidgets>


#include "dragwidget.h"
#include "mymenu.h"
#include "draglabel.h"
#include "QPushButton"
#include "sysinterface.h"
#include <QStyleOptionGraphicsItem>



#define EXTENDED_LAYOUT 0
//! [0]
//!
QString qs_imagepath[] = {
                        ":/images/boat.png",
                        ":/images/car.png",
                         ":/images/house.png",
                        ":/images/boat.png",
                        ":/images/car.png",
                        ":/images/house.png",
                        ":/images/boat.png",
                        ":/images/car.png",
                         ":/images/house.png",
                        ":/images/boat.png",
                        ":/images/car.png",
                        ":/images/house.png"
                        };

QString qs_connType[] = {
                         "VGA-0",
                        "DVI-0-0",
                        "DVI-0-1",
                        "DVI-1-0",
                        "DVI-1-1",
                        "DisplayPort-0",
                        "DisplayPort-1",
                        "DisplayPort-2",
                        "HDMI-0",
                        "HDMI-1",
                        "VGA-0",
                       "DVI-0-0",
                       "DVI-0-1",
                       "DVI-1-0",
                       "DVI-1-1",
                       "DisplayPort-0",
                       "DisplayPort-1",
                       "DisplayPort-2",
                       "HDMI-0",
                       "HDMI-1"
                      };



DragWidget::~DragWidget(){

    if( m_xmonitor ){
        for(unsigned int i=0;i < m_iNumMonitor * m_iNumMonitor;++i){
            delete m_xmonitor[i];
            m_xmonitor[i]=NULL;
        }
        free (m_xmonitor);
        m_xmonitor=NULL;
    }
}

DragWidget::DragWidget(QWidget *parent)
    : QFrame(parent)
{
// Run time create the UI

//==================================================================================
    //Working system calls for xrandr == SYSTEM INITIALIZATION DONE AT FIRST
    //TODO: parsing and displaying information
    m_pSystemInterface = new SysInterface;
//    QString displaydata = p->read_display_configuration();

//    QStringList screens = displaydata.split("Screen ");
    m_iNumMonitor = m_pSystemInterface->getConnected();
    m_iTotalConnectors = m_pSystemInterface->getTotalConnectors();

    //iNumMonitor = p->getiNumMonitors();
//==================================================================================
    //m_iNumMonitor  = 3;

    pDataItemModel= NULL;

    GRID_WIDTH = 100;
    ICON_WIDTH = 100;
    GRID_HEIGHT = GRID_WIDTH;
    GRID_CELL_PADDING_MULTIPLIER = 0.75f;


    left = new QWidget(this);
    right = new QWidget(this);




    current_label = NULL;

    myMenu = new QMenu(this);
    myMenu->addAction("Show details");
    myMenu->addAction("Rotate clockwise 90");
//    connect( myMenu, SIGNAL(triggered(QAction*)), this, SLOT(rotate_right(QAction*)));

    myMenu->addAction("Rotate anticlockwise 90");

    m_gridlayout = NULL;



    if( EXTENDED_LAYOUT )
    {
        QDesktopWidget q;
        QRect rec = q.availableGeometry();
        int app_width  =(m_iTotalConnectors)*GRID_WIDTH;
        int app_height = (m_iTotalConnectors)*GRID_HEIGHT;

        int w = rec.width()-100;
        int h = rec.height()-100;

        if( app_width > w ) {
            app_width = w;
            int newGridWidth = app_width / m_iTotalConnectors;
            GRID_WIDTH = newGridWidth ;
        }
        if(app_height > h ) {
            app_height = h;
            int newGridHeight = app_height/ m_iTotalConnectors;
            GRID_HEIGHT = newGridHeight ;

        }


        //layout->setColumnMinimumWidth(110,100);
        m_xmonitor = (XMonitorWidget**)malloc ( sizeof (XMonitorWidget*)*m_iTotalConnectors*m_iTotalConnectors);
        m_max_y= m_max_x = (m_iTotalConnectors)*GRID_WIDTH;

        setMinimumSize( app_width, app_height ); // creating the indvidual box
        setFrameStyle(QFrame::Sunken | QFrame::StyledPanel);
        setAcceptDrops(true);



        //First time grid layout creation

        createXGridLayout();


    }else {

        int app_width  =(m_iNumMonitor*6)*GRID_WIDTH;
        int app_height = (m_iNumMonitor*4)*GRID_HEIGHT;

        //layout->setColumnMinimumWidth(110,100);
        m_xmonitor = (XMonitorWidget**)malloc ( sizeof (XMonitorWidget*)*m_iNumMonitor*m_iNumMonitor);
        m_max_y= m_max_x = (m_iNumMonitor)*GRID_WIDTH;

        setMinimumSize( app_width, app_height ); // creating the indvidual box
        setFrameStyle(QFrame::Sunken | QFrame::StyledPanel);
        setAcceptDrops(true);


        //First time grid layout creation

        createMinGridLayout();

    }

    GRID_CELL_TEXTLEN = ICON_WIDTH * GRID_CELL_PADDING_MULTIPLIER ;


    QHBoxLayout* hlayout = new QHBoxLayout();



    QSizePolicy szp;
    szp.setVerticalStretch(0);

    left->setSizePolicy(szp);

    hlayout->addWidget(left, 0, Qt::AlignLeft | Qt::AlignTop );
    hlayout->addWidget(right);



    QGridLayout* table_grid = new QGridLayout(right);
    m_pTableview = new QTableView(right);

    generate_table_view(0);


    m_pTableview->setColumnWidth(0,150);
    m_pTableview->setColumnWidth(1, 600);
    //m_pTableview->setWordWrap(true);



    hlayout->addWidget( m_pTableview );

    this->setLayout(hlayout);

    m_iconWidth=m_vmonitor[0]->width();
    m_iconHeight=m_vmonitor[0]->height();

    highlightedRect = QRect();
    inPlace = 0;



    update();

}


void DragWidget::generate_table_view( int index )
{

    if(index < 0 )
        return;

    if(NULL == pDataItemModel)
        pDataItemModel = new QStandardItemModel(1, 2, this );

    m_pTableview->setModel(pDataItemModel);

    pDataItemModel->setHorizontalHeaderItem(0, new QStandardItem("Header-1"));
    pDataItemModel->setHorizontalHeaderItem(1, new QStandardItem("Header-2"));

    QStandardItem *keyitem = new QStandardItem( QString("Name") );
    pDataItemModel->setItem( 0, 0, keyitem );

    QStandardItem *keyvalue = new QStandardItem( m_Vxmonitor[index]->getDisplayName() );
    pDataItemModel->setItem( 0 , 1, keyvalue );

    //pDataItemModel->removeRows(0, pDataItemModel->rowCount());

    QMap<QString, QString> qMap = m_Vxmonitor[index]->getXMonitorProperties();
    QMapIterator<QString, QString> iter(qMap);

    QList<QStandardItem*> qlst;

    while(iter.hasNext())
    {
        iter.next();
        qlst.clear();

        QStandardItem *keyitem = new QStandardItem( iter.key().trimmed() );
        //pDataItemModel->setItem( index , 0, item );

        QStandardItem *keyvalue = new QStandardItem( iter.value().trimmed() );
        //pDataItemModel->setItem( index , 1, item);

        qlst << keyitem;
        qlst << keyvalue;
        pDataItemModel->appendRow( qlst );

        m_pTableview->resizeRowToContents( index );

        index++;

    }

    //this->update();

    return;

}

void DragWidget::regenerate_table_view( int index )
{


    if(index < 0 || index > m_Vxmonitor.size() )
        return;

    if( NULL != pDataItemModel ){
        delete pDataItemModel;
        pDataItemModel = NULL;
        generate_table_view(index);
    }


    return;

}

void DragWidget::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }
}

void DragWidget::dragMoveEvent(QDragMoveEvent *event)
{
    QRect updateRect = highlightedRect.united(targetSquare(event->pos()));
    if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
        if (event->source() == this) {
            highlightedRect = targetSquare(event->pos());
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } /*else {
            event->acceptProposedAction();
        }*/
    } else {
        highlightedRect = QRect();

        event->ignore();
    }

    update(updateRect);

}

void DragWidget::dropEvent(QDropEvent *event)
{

   QPoint event_at = event->pos();
   if( event_at.x() >= m_max_x ||
        event_at.y() >= m_max_x ){

        highlightedRect = QRect();
        event->ignore();
        return;
    }

    if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
        QByteArray itemData = event->mimeData()->data("application/x-dnditemdata");
        QDataStream dataStream(&itemData, QIODevice::ReadOnly);
        QRect square = targetSquare(event->pos());
        QPixmap pixmap;
        QPoint offset;
        dataStream >> pixmap >> offset;


        // at == to
        //from == from

        int grid_index_col_to = event_at.x() / GRID_WIDTH;
        int grid_index_row_to = event_at.y() / GRID_HEIGHT;

        QPoint fromPosEvent = m_pos_current_label ;
        int grid_index_col_from = fromPosEvent.x() / GRID_WIDTH;
        int grid_index_row_from = fromPosEvent.y() / GRID_HEIGHT;


        int from_index_in_vector = (m_iNumMonitor* grid_index_row_from)+grid_index_col_from ;

        int to_index_in_vector =  (m_iNumMonitor* grid_index_row_to)+grid_index_col_to ;


        XMonitorWidget* qwfrom = m_Vxmonitor.at(from_index_in_vector );
        XMonitorWidget* qwto = m_Vxmonitor.at( to_index_in_vector );

        XMonitorWidget* tmp1 = new XMonitorWidget(qwfrom->getImagePAth(),
                                                  "N/A",
                                                  GRID_CELL_TEXTLEN,
                                                   qwfrom->getMonitorText() );
        tmp1->setCurrentVectorIndex(to_index_in_vector);
        tmp1->setMatrixPosition(QPoint(grid_index_col_to, grid_index_row_to));
        m_xmonitor[to_index_in_vector] = tmp1;

        m_vmonitor.push_back( (QLabel*)m_xmonitor[to_index_in_vector]->getXWidget() );
        m_Vxmonitor.replace(to_index_in_vector, m_xmonitor[to_index_in_vector] );


        tmp1->resetXWidget(GRID_CELL_TEXTLEN);
        qwfrom->hide();
        delete qwfrom;
        qwfrom = NULL;


        XMonitorWidget* tmp2 = new XMonitorWidget(qwto->getImagePAth(),
                                                  "N/A",
                                                  GRID_CELL_TEXTLEN,
                                                  qwto->getMonitorText());
        tmp2->setCurrentVectorIndex(from_index_in_vector);
        tmp2->setMatrixPosition(QPoint(grid_index_col_from, grid_index_row_from));
        m_xmonitor[from_index_in_vector] = tmp2;

        m_vmonitor.replace( from_index_in_vector, (QLabel*)m_xmonitor[from_index_in_vector]->getXWidget() );
        m_Vxmonitor.replace(from_index_in_vector, m_xmonitor[from_index_in_vector] );

        tmp2->resetXWidget(GRID_CELL_TEXTLEN);
        qwto->hide();
        delete qwto;
        qwto= NULL;


//This action is needed if grid is already created and we need to recreate it
// Bug in QT forced us to recreate the grid on every swap/drag

        RecreateMinXGridLayout();

        update();
     //  m_gridlayout->update();

        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            highlightedRect = QRect();
            update(square);
            event->accept();

        } else {
            event->acceptProposedAction();
        }
    } else {
        highlightedRect = QRect();
        event->ignore();
    }
}

void DragWidget::createMinGridLayout(){

    if( NULL==m_gridlayout )
    {
     // boxlayout->addItem(m_gridlayout);

       m_gridlayout = new QGridLayout(left);

       QStringList qstlst = m_pSystemInterface->getConnectedMonitorsFirst();

        int index=-1;
        for( unsigned int row=0; row < m_iNumMonitor; ++row ){
            for( unsigned int i=0; i < m_iNumMonitor; ++i ){

                index++;

                XMonitorWidget* tmp = new XMonitorWidget(qs_imagepath[i],
                                                         "CrashFix",
                                                         GRID_CELL_TEXTLEN,
                                                         qstlst[index] );

                m_xmonitor[index] = tmp;


                m_xmonitor[index]->setCurrentVectorIndex(index);
                m_xmonitor[index]->setMatrixPosition(QPoint(i, row));

                m_vmonitor.push_back( (QLabel*)m_xmonitor[index]->getXWidget() );
                m_Vxmonitor.push_back(  m_xmonitor[index] );

                m_gridlayout->addWidget(m_xmonitor[index]->getXWidget(), row, i);

            }

        }
    }


}

void DragWidget::createXGridLayout(){

    if( NULL==m_gridlayout )
    {
       m_gridlayout = new QGridLayout(left);

        int index=-1;
        for( unsigned int row=0; row<m_iTotalConnectors; ++row ){
            for( unsigned int i=0; i<m_iTotalConnectors; ++i ){

                index++;

                //XMonitor* tmp = new XMonitor(qs_imagepath[i],  qs_connType[index], GRID_CELL_TEXTLEN);
                XMonitorWidget* tmp = new XMonitorWidget(qs_imagepath[i],  "CrashFix", GRID_CELL_TEXTLEN, NULL );
                m_xmonitor[index] = tmp;//->getNewXMonitor(i);

                m_xmonitor[index]->setCurrentVectorIndex(index);
                m_xmonitor[index]->setMatrixPosition(QPoint(i, row));

                m_vmonitor.push_back( (QLabel*)m_xmonitor[index]->getXWidget() );
                m_Vxmonitor.push_back(  m_xmonitor[index] );
                m_gridlayout->addWidget(m_xmonitor[index]->getXWidget(), row, i);

            }

        }
    }


}

void DragWidget::RecreateMinXGridLayout(){

    if( NULL != m_gridlayout )
    {
        delete m_gridlayout;
        m_gridlayout = NULL;
    }

    m_gridlayout = new QGridLayout(left);
    if( NULL == m_gridlayout )
        return;


        QVectorIterator<XMonitorWidget*> itr( m_Vxmonitor);

        int row = 1, col = 1, index = 1;

        while (itr.hasNext()){

{
            XMonitorWidget* tmp = itr.next();


//            QWidget* widg = tmp->redrawXWidget();//tmp->getXWidget();
//            m_gridlayout->addWidget( widg, row-1, col-1);
            QLabel *child =  tmp->rotateXWidget() ;
//            QPixmap pixmap(*child->pixmap());
//            QMatrix rm;
//            rm.rotate(tmp->getRotationAngle());
//            pixmap = pixmap.transformed(rm);

//            child->close();
//            delete child;
//            child = NULL;

//            child = new QLabel(left);
//            child->setPixmap(pixmap);
//            child->setStyleSheet("border: 5px solid grey");
//            child->show();
//            child->setAttribute(Qt::WA_DeleteOnClose);

//            tmp->setXwidget(child);
            //tmp->setRotationAngle(rotate_angle);
            //int row1 = m_pos_current_label.y()/GRID_HEIGHT;
            //int col1 = m_pos_current_label.x()/GRID_WIDTH ;

            if( NULL != child )
                m_gridlayout->addWidget( (QWidget*)child, row-1, col-1);

}

            col++;

            if(index%m_iNumMonitor <= 0 ){
                row++;
                col=1;

            }


               index++;

        }

        m_gridlayout->update();


}

void DragWidget::RecreateXGridLayout(){

    if( NULL != m_gridlayout )
    {
        delete m_gridlayout;

        m_gridlayout = new QGridLayout(left);


        QVectorIterator<XMonitorWidget*> itr( m_Vxmonitor);

        int row = 1, col = 1, index = 1;

         int sz = m_Vxmonitor.size();

        while (itr.hasNext()){

            XMonitorWidget* temp = itr.next();
            QWidget* widg = temp->getXWidget();
            m_gridlayout->addWidget( widg, row-1, col-1);
            col++;

            if(index%m_iNumMonitor <= 0 ){
                row++;
                col=1;

            }


               index++;

        }

        m_gridlayout->update();
    }

}

const QRect DragWidget::targetSquare(const QPoint &position) const
{
    return QRect(position.x()/m_iconHeight * m_iconHeight, position.y()/m_iconWidth * m_iconWidth, m_iconWidth, m_iconWidth);
}
//! [1]
void DragWidget::mousePressEvent(QMouseEvent *event)
{

    QLabel *child = static_cast<QLabel*>(childAt(event->pos()));
    if (!child)
        return;

    current_label = child;
    m_pos_current_label = event->pos();
    int row = m_pos_current_label.y()/GRID_HEIGHT;
    int col = m_pos_current_label.x()/GRID_WIDTH ;
    uint pos_vector = (m_iNumMonitor*row) + col;

    switch( event->button() )
    {

        case Qt::RightButton:
        {
            mapToGlobal(event->globalPos());
            QAction* selectedItem = myMenu->exec(event->globalPos());
            if (selectedItem)
            {
                int rotate_angle = 0;


                QString qtemp = selectedItem->text();
                if( qtemp == "Show details" ){
                    update_table_view(QPoint(row,col));
                    return;
                }
                else if( qtemp == "Rotate clockwise 90" )
                {
                    rotate_angle = 90;
                }else if( qtemp == "Rotate anticlockwise 90" )
                {
                    rotate_angle = -90;
                }
                else
                    return;

                rotate_widget( pos_vector, rotate_angle );


            }
            else
            {
                // nothing was chosen
                //MessageBoxA(NULL, "nothing is selectedItem.", NULL, 0 );
            }
        }
        return;


    case Qt::LeftButton:
        {


                QPixmap pixmap = *child->pixmap();

            QByteArray itemData;
            QDataStream dataStream(&itemData, QIODevice::WriteOnly);
            dataStream << pixmap << QPoint(event->pos() - child->pos());
        //! [1]

        //! [2]
            QMimeData *mimeData = new QMimeData;
            mimeData->setData("application/x-dnditemdata", itemData);
        //! [2]

        //! [3]
            QDrag *drag = new QDrag(this);
            drag->setMimeData(mimeData);
            drag->setPixmap(pixmap);
            drag->setHotSpot(event->pos() - child->pos());
        //! [3]

            QPixmap tempPixmap = pixmap;
            QPainter painter;
            painter.begin(&tempPixmap);
            painter.fillRect(pixmap.rect(), QColor(127, 127, 127, 127));
            painter.end();

            child->setPixmap(tempPixmap);

            if (drag->exec(Qt::CopyAction | Qt::MoveAction, Qt::CopyAction) == Qt::MoveAction) {
                child->close();
            } else {
                child->show();
                child->setPixmap(pixmap);
            }
        }
        return;

    case Qt::MiddleButton:
        return;
    default:
        return;

    }


}

//useful for drawing pink rectangle
void DragWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.fillRect(event->rect(), Qt::white);

    if (highlightedRect.isValid()) {
        painter.setBrush(QColor("#ffcccc"));
        painter.setPen(Qt::NoPen);
        painter.drawRect(highlightedRect.adjusted(0, 0, -1, -1));
    }

    painter.end();
}

void DragWidget::update_table_view( QPoint qpt){

    //Find the vector index
    int index = qpt.x() *m_iNumMonitor + qpt.y();


    if( index < m_Vxmonitor.size() && m_Vxmonitor[index]->IsConnected() )
    {

        regenerate_table_view( index );
    }
    else
    {
        //null pointer no useful data
        //reset_tableview();
    }

}

void DragWidget::rotate_widget( uint pos_vector, int rotate_angle ){

    XMonitorWidget* tmp = m_Vxmonitor.at(pos_vector);
    tmp->setRotationAngle(rotate_angle);

//
//    QLabel *child = static_cast<QLabel*>(tmp->getXWidget());
//    QPixmap pixmap(*child->pixmap());
//    QMatrix rm;
//    rm.rotate(rotate_angle);
//    pixmap = pixmap.transformed(rm);

//    child->close();
//    delete child;
//    child = NULL;

//    child = new QLabel(left);
//    child->setPixmap(pixmap);
//    child->setStyleSheet("border: 5px solid grey");
//    child->show();
//    child->setAttribute(Qt::WA_DeleteOnClose);

//    tmp->setXwidget(child);
//    tmp->setRotationAngle(rotate_angle);
//    int row1 = m_pos_current_label.y()/GRID_HEIGHT;
//    int col1 = m_pos_current_label.x()/GRID_WIDTH ;
//    //m_gridlayout->addWidget(child, row1, col1 );


    RecreateMinXGridLayout();

}

