#include "xmonitor.h"


const float GRID_CELL_PADDING_MULTIPLIER = 0.75f;


XMonitorWidget::XMonitorWidget(QWidget *parent) :
    QWidget(parent)
{
    m_pix=NULL;
    m_qpt = NULL;

    m_pLabel = NULL;
    m_pXMonitorInfo = NULL;

}

XMonitorWidget::XMonitorWidget(QString imagepath, QString displayname, int len, QString text ) :
        m_ImagePath(imagepath) ,
       // m_DisplayName(displayname) ,
        GRID_CELL_TEXTLEN(len),
        m_qsMonitorText(text)

{
//
    m_pix = NULL;
    m_qpt = NULL;
    m_pLabel = NULL;
    m_DisplayName = "";
    m_Pos_in_matrix = QPoint(-1,-1);
    m_iAngle = 0;
    m_iCurrVectorIndex = -1;
    m_pXMonitorInfo = NULL;
//

    m_pix = new QPixmap(imagepath);
    if(NULL==m_pix)
        return;
    else
    {
        //labels[i]->setScaledContents(true);
        m_qpt = new QPainter(m_pix);
        if(NULL==m_qpt)
            return;
        m_qpt->setFont(QFont("Arial"));
    }


    if( NULL == m_pXMonitorInfo ){
        m_pXMonitorInfo = new XMonitorInfo( m_qsMonitorText );
    }
    else
        m_pXMonitorInfo->GenerateMonitorDB(m_qsMonitorText);


    m_DisplayName = m_pXMonitorInfo ->getDisplayName();

    resetXWidget(GRID_CELL_TEXTLEN);

}


XMonitorWidget::~XMonitorWidget()
{
    //Freeing following resource causing application segment error..

    //if(m_pix){delete m_pix; m_pix=NULL;}
    //if(m_qpt){delete m_qpt; m_qpt=NULL;}
  //  if(m_pLabel){delete m_pLabel; m_pLabel=NULL;}


}

XMonitorWidget* XMonitorWidget::getNewXMonitor( void ){ return this; }

void XMonitorWidget::resetXWidget(int len ){

    if(NULL==m_pLabel)
        m_pLabel = new QLabel;


    //Following is the font size adjustment to fit into the label width

    int pixel_wide = m_qpt->fontMetrics().width(m_DisplayName);
    float factor = GRID_CELL_TEXTLEN / (float)pixel_wide;

    if( pixel_wide > GRID_CELL_TEXTLEN ){

        //float factor = 80.0f / (float)pixel_wide ;
        QFont f = m_qpt->font();

        float fsize = f.pointSizeF();
        qreal scale_font = fsize* factor * GRID_CELL_PADDING_MULTIPLIER;
        f.setPointSizeF((qreal) scale_font );
        f.setFamily("Arial");
        m_qpt->setFont(f);

     }


    //Following call forces to use multiple instances of painter.
    //todo: if we can chage the drawing text for the same painter we should able to use
    //single instalce of painter.
    m_qpt->drawText(10,10, m_DisplayName);

    m_pLabel->setStyleSheet("border: 5px solid grey");
    m_pLabel->setPixmap(*m_pix);
    m_pLabel->show();
    m_pLabel->setAttribute(Qt::WA_DeleteOnClose);

}

QMap<QString, QString> XMonitorWidget::getXMonitorProperties(){

    return m_pXMonitorInfo->getProperties();
}


bool XMonitorWidget::IsConnected(){
    return m_pXMonitorInfo->IsConnected();
}



QLabel* XMonitorWidget::rotateXWidget( void )
{

   // QPixmap pixmap( m_ImagePath );

    if(NULL==m_pix)
       m_pix = new QPixmap(m_ImagePath);
    else
    {
        //labels[i]->setScaledContents(true);
        if(NULL==m_qpt)
            m_qpt = new QPainter(m_pix);

        m_qpt->setFont(QFont("Arial"));
    }

    QMatrix rm;
    rm.rotate( m_iAngle );
    QPixmap pixmap = m_pix->transformed(rm);


    int pixel_wide = m_qpt->fontMetrics().width(m_DisplayName);
    float factor = GRID_CELL_TEXTLEN / (float)pixel_wide;

    if( pixel_wide > GRID_CELL_TEXTLEN ){

        //float factor = 80.0f / (float)pixel_wide ;
        QFont f = m_qpt->font();

        float fsize = f.pointSizeF();
        qreal scale_font = fsize* factor * GRID_CELL_PADDING_MULTIPLIER;
        f.setPointSizeF((qreal) scale_font );
        f.setFamily("Arial");
        m_qpt->setFont(f);

     }


    //Following call forces to use multiple instances of painter.
    //todo: if we can chage the drawing text for the same painter we should able to use
    //single instalce of painter.
    m_qpt->drawText(10,10, m_DisplayName);


    if(NULL==m_pLabel)
        m_pLabel = new QLabel;

    m_pLabel->setPixmap(pixmap);
    m_pLabel->setStyleSheet("border: 5px solid grey");

    m_pLabel->show();
    m_pLabel->setAttribute(Qt::WA_DeleteOnClose);

    return m_pLabel;

}
