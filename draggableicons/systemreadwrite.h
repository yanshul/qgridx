#ifndef SYSTEMREADWRITE_H
#define SYSTEMREADWRITE_H

#include <QObject>
#include <QProcess>
#include <QThread>

const QString XCOMMAND="xrandr";


class SystemReadWrite
{
    QProcess *m_pProcess ;
public:
    SystemReadWrite();
    ~SystemReadWrite();
    bool ExecuteCommand(QString command, QStringList arguments, QString& retData);
    bool ExecuteWaitCommand(QString command, QStringList arguments, QString& retData);
};

#endif // SYSTEMREADWRITE_H
