#ifndef COMMON_H
#define COMMON_H




unsigned int GRID_WIDTH = 100;
unsigned int ICON_WIDTH = 100;
unsigned int GRID_HEIGHT = GRID_WIDTH;
const float GRID_CELL_PADDING_MULTIPLIER = 0.75f;
const float GRID_CELL_TEXTLEN = ICON_WIDTH * GRID_CELL_PADDING_MULTIPLIER ;



#endif // COMMON_H
