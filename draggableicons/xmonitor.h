#ifndef XMONITOR_H
#define XMONITOR_H

#include <QWidget>
#include <QLabel>
#include <QPainter>
#include <QPixmap>


#include "xmonitorinfo.h"


class XMonitorWidget : public QWidget
{
    Q_OBJECT


    QPixmap* m_pix ;
    QPainter* m_qpt ;
    QLabel *m_pLabel ;
    QString m_ImagePath;
    QString m_DisplayName;
    QPoint m_Pos_in_matrix;

    qint32 m_iAngle;
    qint32 m_iCurrVectorIndex ;

    int GRID_CELL_TEXTLEN;
    QString m_qsMonitorText;
    XMonitorInfo* m_pXMonitorInfo;


public:
    explicit XMonitorWidget(QWidget *parent = 0);
    explicit XMonitorWidget(QString imagepath, QString displayname, int len, QString text );

    ~XMonitorWidget();
// FEtching entities

    XMonitorWidget* getNewXMonitor(void);

    QWidget* getXWidget() { return m_pLabel; }
    QString getMonitorText(){ return m_qsMonitorText; }
    QString getDisplayName(){ return m_DisplayName;}
    QString getImagePAth() { return m_ImagePath; }
    QPoint getMatrixPosition() { return m_Pos_in_matrix; }
    qint32 getCurrentVectorIndex () { return m_iCurrVectorIndex ; }
    qint32 getRotationAngle () { return m_iAngle; }
    QMap<QString, QString> getXMonitorProperties();

    //SEtting
    void setXwidget(QWidget* label){ m_pLabel = static_cast<QLabel*>(label);}
    void setRotationAngle( int x ) { m_iAngle += x; }
    QLabel* rotateXWidget( void );
    void setCurrentVectorIndex( qint32 index ) { m_iCurrVectorIndex = index; }
    void setMatrixPosition( QPoint loc ) { m_Pos_in_matrix = loc; }
    void resetXWidget(int);

    bool IsConnected();

signals:

public slots:

};

#endif // XMONITOR_H
