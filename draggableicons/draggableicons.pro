QT += widgets

HEADERS     = dragwidget.h \
    draglabel.h \
    sysinterface.h \
    systemreadwrite.h \
    xmonitor.h \
    common.h \
    xmonitorinfo.h
RESOURCES   = draggableicons.qrc
SOURCES     = dragwidget.cpp \
              main.cpp \
    draglabel.cpp \
    systemreadwrite.cpp \
    sysinterface.cpp \
    xmonitor.cpp \
    xmonitorinfo.cpp

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/draganddrop/draggableicons
INSTALLS += target
