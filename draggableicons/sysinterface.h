#ifndef SYSINTERFACE_H
#define SYSINTERFACE_H



#include <xmonitorinfo.h>
#include "systemreadwrite.h"
#include <QStringList>


//const QString XCOMMAND="xrandr";

class SysInterface
{


    SystemReadWrite* m_pSystemRW;
    QStringList m_parseOutputData;

public:
    SysInterface();
    QString read_display_configuration();
    QStringList getXInitString() { return m_parseOutputData;    }
    int getScreenCount();
    QStringList getConnectedMonitors();
    QStringList construct_meaningful_strings( QString temp );
    int getConnected( );
    QStringList getConnectedMonitorsFirst( );
    int getTotalConnectors( );
    QString getXMonitorString(int index);
};

#endif // SYSINTERFACE_H
