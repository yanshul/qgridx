#ifndef XMONITORINFO_H
#define XMONITORINFO_H



#include <QStringList>
#include <QMap>

const QString SPACER = " ";
const QString SEARCH_STRING="connected" + SPACER;
const QString NEWLINE_STRING="\n";

class XMonitorInfo
{

    QMap <QString, QString> m_mapInfo;
    QString m_DisplayName;
    QString m_qsRaw;
    QStringList m_qslParsedRaw;

public:
    XMonitorInfo();
    XMonitorInfo( QString text );
    void setRaw(QString text){ m_qsRaw = text;}

    void GenerateMonitorDB( QString );
    QString getDisplayName( );
    void createDataInfoMap();
    QMap <QString, QString> getProperties(){ return m_mapInfo;}
    bool IsConnected();

};

#endif // XMONITORINFO_H
