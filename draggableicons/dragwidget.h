/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DRAGWIDGET_H
#define DRAGWIDGET_H

#include <QFrame>
#include <QLabel>
#include <QMenu>
#include <QGridLayout>
#include<QTableView>
#include <QStandardItemModel>

#include <xmonitor.h>

#include "sysinterface.h"


QT_BEGIN_NAMESPACE
class QDragEnterEvent;
class QDropEvent;
QT_END_NAMESPACE

//! [0]
class DragWidget : public QFrame
{

    Q_OBJECT

public:
    DragWidget(QWidget *parent = 0);
    ~DragWidget();
    //void drawhouse(QPoint pos);

protected:
    void dragEnterEvent(QDragEnterEvent *event) Q_DECL_OVERRIDE;
    void dragMoveEvent(QDragMoveEvent *event) Q_DECL_OVERRIDE;
    void dropEvent(QDropEvent *event) Q_DECL_OVERRIDE;

    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
    void createMinGridLayout();
    void createXGridLayout();
    void RecreateXGridLayout();
    void RecreateMinXGridLayout();
    void generate_table_view( int index );
    void regenerate_table_view( int index );
    void rotate_widget( uint pos_vector, int rotate_angle );

private slots:
    //void rotate_right(QAction*);

//    void on_label_customContextMenuRequested(const QPoint &pos);

private:

    QWidget* right;
    QWidget* left ;

    unsigned int GRID_WIDTH ;
    unsigned int ICON_WIDTH ;
    unsigned int GRID_HEIGHT;
    float GRID_CELL_PADDING_MULTIPLIER ;
    float GRID_CELL_TEXTLEN ;


    SysInterface* m_pSystemInterface ;
    XMonitorWidget** m_xmonitor;
    int m_iNumMonitor;
    int m_iTotalConnectors;

    const QRect targetSquare(const QPoint &position) const;
    QRect highlightedRect;
    int inPlace;
    int m_ImageSize;
    int m_iconHeight;
    int m_iconWidth;
    unsigned int m_max_x;
    unsigned int m_max_y;

    QPoint m_pos_current_label ;
    QPoint *globalPos;
    QLabel *houseIcon;
    QLabel *boatIcon;
    QLabel *carIcon;

    QGridLayout* m_gridlayout ;
    QTableView* m_pTableview ;

    QVector <QLabel*> m_vmonitor;
    QVector <XMonitorWidget*> m_Vxmonitor;
    void update_table_view(QPoint qpt);
    QMenu *myMenu;
    QLabel *current_label;
    QStandardItemModel* pDataItemModel;


};
//! [0]

#endif // DRAGWIDGET_H
