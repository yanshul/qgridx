#include "draglabel.h"

#include <QPainter>

DraggabelLables::DraggabelLables(QWidget *parent)
    : QLabel(parent)
{

}

DraggabelLables::DraggabelLables(const QString &text, QWidget *parent)
: QLabel(text, parent)
{
}

void DraggabelLables::paintEvent(QPaintEvent*)
{
//    static int rotation_angle;
//    QPainter painter(this);
//    painter.setPen(Qt::black);
//    painter.setBrush(Qt::Dense1Pattern);

//    painter.rotate(0);
//    //rotation_angle += (rotation_angle > 360 ? 0: 45);

//    painter.drawText(0,0, text());
}

QSize DraggabelLables::minimumSizeHint() const
{
    QSize s = QLabel::minimumSizeHint();
    return QSize(s.height(), s.width());
}

QSize DraggabelLables::sizeHint() const
{
    QSize s = QLabel::sizeHint();
    return QSize(s.height(), s.width());
}
