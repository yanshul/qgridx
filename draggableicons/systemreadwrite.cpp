#include "systemreadwrite.h"
#include <QStringList>


SystemReadWrite::SystemReadWrite()
{

    m_pProcess = new QProcess( );
}

SystemReadWrite::~SystemReadWrite()
{
    if(m_pProcess){
        delete m_pProcess ;
        m_pProcess = NULL;
    }
}



bool SystemReadWrite::ExecuteWaitCommand(QString command, QStringList arguments, QString& retData){


    m_pProcess->start(command, arguments);
    m_pProcess->waitForFinished();
    QString output(m_pProcess->readAllStandardOutput());

    retData = output;

    return (m_pProcess->error() >=0 ? true: false);
}


bool SystemReadWrite::ExecuteCommand(QString command, QStringList arguments, QString& retData){


    m_pProcess->start(command, arguments);
    QString output(m_pProcess->readAllStandardOutput());

    retData = output;

    return (m_pProcess->error() >=0 ? true: false);
}
