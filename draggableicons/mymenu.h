#ifndef MYMENU_H
#define MYMENU_H
#include <QMenu>

class Menu : public QMenu
{
    Q_OBJECT
public:
    explicit Menu(QWidget *parent = 0);

signals:

public slots:
void showMenu(const QPoint &pos);
};
#endif // MYMENU_H
