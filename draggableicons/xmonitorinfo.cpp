#include "xmonitorinfo.h"


XMonitorInfo::XMonitorInfo()
{
}



XMonitorInfo::XMonitorInfo(QString text ): m_qsRaw(text){


    GenerateMonitorDB(m_qsRaw);

}

void XMonitorInfo::GenerateMonitorDB(QString text)
{
    m_qslParsedRaw = text.split("\n\t");
    for(int i=0; i< m_qslParsedRaw.count(); ++i )
        m_qslParsedRaw[i].remove("\t");

    int sz = m_qslParsedRaw.count();

    createDataInfoMap();
}


void XMonitorInfo::createDataInfoMap(){

    QStringList qstlst = m_qslParsedRaw;

    QString value="", property="";

    int i=0;
    while ( i < qstlst.count() )
    {


        QString tmp = qstlst[i];
        int pos0 = tmp.indexOf(":");
        if( pos0 != -1 )
        {
            property = tmp.left(pos0).trimmed();
            value = tmp.right(tmp.size() - pos0 - 2 ).trimmed();

            m_mapInfo.insert(property,value);

            //value = "";
            //property="";
        }
        else{

            value += NEWLINE_STRING + tmp.right(tmp.size() ).trimmed();
            if(!m_mapInfo.isEmpty())
                m_mapInfo.find(property).value() = value;
        }


        ++i;
    }

}


QString XMonitorInfo::getDisplayName(){

    int index=-1;

    QString tmp ;

    while(  !m_qslParsedRaw[++index].contains(SEARCH_STRING) && index < m_qslParsedRaw.length()-1 );


    tmp = m_qslParsedRaw[index];



    int ipos = tmp.indexOf(SEARCH_STRING);


    tmp = tmp.left( ipos ) ;
    int ipos1 = tmp.lastIndexOf( NEWLINE_STRING );

    m_DisplayName = tmp.right( tmp.size() - ipos1 - NEWLINE_STRING.size()).trimmed() ;
    int sz = m_DisplayName.size();

    return m_DisplayName;

}


bool XMonitorInfo::IsConnected()
{
    return !m_qsRaw.contains("disconnected");
}
